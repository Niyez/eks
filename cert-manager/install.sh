#Installing with regular manifests 
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.0/cert-manager.yaml

#Installing with Helm
#Create the namespace for cert-manager:
kubectl create namespace cert-manager

#Add the Jetstack Helm repository:
helm repo add jetstack https://charts.jetstack.io

#Update your local Helm chart repository cache:
helm repo update

# cert-manager requires a number of CRD resources to be installed into your cluster as part of installation.

# This can either be done manually, using kubectl, or using the installCRDs option when installing the Helm chart.

# Note: If you’re using a helm version based on Kubernetes v1.18 or below (Helm v3.2) installCRDs will not work with cert-manager v0.16. For more info see the v0.16 upgrade notes

# Option 1: installing CRDs with kubectl
# Install the CustomResourceDefinition resources using kubectl:
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.0/cert-manager.crds.yaml

# Option 2: install CRDs as part of the Helm release
# To automatically install and manage the CRDs as part of your Helm release, you must add the --set installCRDs=true flag to your Helm installation command.

# Uncomment the relevant line in the next steps to enable this.

# To install the cert-manager Helm chart:

  helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.4.0 \
  # --set installCRDs=true

#Reference link
https://cert-manager.io/docs/installation/kubernetes/